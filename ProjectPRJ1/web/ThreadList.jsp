<%-- 
    Document   : ThreadList
    Created on : Jul 8, 2022, 4:40:08 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/home.css">
        <link rel="stylesheet" href="CSS/threadList.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <c:if test="${sessionScope.account != null}">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-dark" id="newThreadBtn" href="addThread">Create a new thread</a>  
                    </div>
                </div>
            </div>
        </c:if>


        <br/>
        <div class="container-md">
            <div class="row listTable">
                    <div class="col-1 id">
                        <p>ID</p>
                    </div>
                    <div class="col-5 tName"> Thread Name</div>
                    <div class="col-2 tCreator"> <p>Creator</p></div>
                    <div class="col-2 tDate"> <p>Date Created</p></div>
                    <div class="col-1 tReply"> <p>Replies</p></div>
                </div>
            <c:forEach items="${requestScope.data}" var="t">
                <div class="row listTable">
                    <div class="col-1 id">
                        <p> ${t.threadID}</p>
                    </div>
                    <div class="col-5 tName"> <a href="threads?tid=${t.threadID}">${t.threadName}</a></div>
                    <div class="col-2 tCreator"> <p>${t.creatorID.username}</p></div>
                    <div class="col-2 tDate"> <p>${t.createdDate}</p></div>
                    <div class="col-1 tReply"> <p>${t.totalReplies}</p></div>
                </div>

            </c:forEach>

        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>
