<%-- 
    Document   : updateComposer
    Created on : Jul 12, 2022, 9:14:31 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/header.css">
        <link rel="stylesheet" href="CSS/updateComposer.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-md">
            <div class="row">
                <c:set var="c" value="${requestScope.composer}"/>
                <c:if test="${sessionScope.account.username != requestScope.composer.adderID.username}">
                    <h3>You are not allowed to edit this artist</h3>
                </c:if>

                <c:if test="${sessionScope.account.username == requestScope.composer.adderID.username}">
                    <h3>Edit information of your uploaded artist.</h3>
                    <div class="col-xs-12 col-sm-12 col-md-12 mx-auto">
                        <form action="updateComposer" method="post" class="updateForm">
                            Composer's name: <input type="text" name="name" value="${c.composerName}"/><br/>
                            Description: <textarea name="desc" class="descBox">${c.desc}</textarea><br/>
                            Link to images: <input type="text" name="image" value="${c.photo}" class="imageLink"/><br/>
                            <input type="hidden" value="${c.composerID}" name="cid"/>
                            <input type="hidden" value="${c.adderID.username}" name="uploaderID"/>
                            <button type="submit" class="submitbtn btn btn-dark" style="margin:0px 10px;">Submit</button>
                        </form>
                    </c:if>
                </div>
            </div>
        </div>
    </body>
</html>
