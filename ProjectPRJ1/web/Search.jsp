<%-- 
    Document   : Search
    Created on : Jul 12, 2022, 3:06:14 AM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <link rel="stylesheet" href="CSS/header.css">
        <link rel="stylesheet" href="CSS/search.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-2"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <!--                    <div class="col-md-4" id="searchBar">
                                            <div class="input-group">
                                                <form action="search" id="searchs">
                                                    <input type="search" class="form-control rounded searchType" placeholder="Search for something...." aria-label="Search" aria-describedby="search-addon" name="key"/>
                                                                                <button type="button" class="btn btn-outline-primary">Search</button>
                                                    <input type="submit" class="btn btn-outline-primary" value="Search">
                                                </form>
                                            </div>
                                        </div>-->
                    <!--home-->
                    <div class="col-md-2 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-2 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-2 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-2 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-md">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-centered">
                    <p>Search in Forum</p>
                    <form action="search">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Thread Name: </span>
                            </div>
                            <input type="text" class="form-control keyEnter1" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="threadName">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Creator's username: </span>
                            </div>
                            <input type="text" class="form-control keyEnter1" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="CreatorID">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">From Date: </span>
                            </div>
                            <input type="date" name="fromDate"/>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">To Date: </span>
                            </div>
                            <input type="date" name="toDate"/>
                        </div>

                        <input type="hidden" name="sid" value="1"/>
                        <button type="submit" class="submitbtn btn btn-dark" style="margin:0px 10px;">Search</button>
                    </form>
                </div>

                <br/>

                <div class="col-xs-12 col-sm-12 col-md-12 col-centered">
                    <p>Search in Review</p>
                    <form action="search">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Song's name: </span>
                            </div>
                            <input type="text" class="form-control keyEnter2" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="songName">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Composer: </span>
                            </div>
                            <input type="text" class="form-control keyEnter2" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="composerName">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Genre: </span>
                            </div>
                            <input type="text" class="form-control keyEnter2" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="Genre">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Mark: </span>
                            </div>
                            <div class="inputRadio">
                                <input type="radio" name="mark" value="0" checked/> All <span><i class="fa-solid fa-star"></i></span>
                                <input type="radio" name="mark" value="1"/> 0-1 <span><i class="fa-solid fa-star"></i></span>
                                <input type="radio" name="mark" value="2"/> 1-2 <span><i class="fa-solid fa-star"></i></span>
                                <input type="radio" name="mark" value="3"/> 2-3 <span><i class="fa-solid fa-star"></i></span>
                                <input type="radio" name="mark" value="4"/> 3-4 <span><i class="fa-solid fa-star"></i></span>
                                <input type="radio" name="mark" value="5"/> 4-5 <span><i class="fa-solid fa-star"></i></span>   
                            </div>
                        </div>

                        <input type="hidden" name="sid" value="2"/>
                        <button type="submit" class="submitbtn btn btn-dark" style="margin:0px 10px;">Search</button>
                    </form>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <div class="container-md">
            <div class="row">
                <c:if test="${requestScope.searchId==1}">
                    <h3>Forum search result:</h3>
                    <c:if test="${requestScope.data1 == null}"><h3>No result</h3></c:if>
                    <c:forEach items="${requestScope.data1}" var="t">
                        <div class="row line">
                            <div class="col-md-10 link">
                                <a href="threads?tid=${t.threadID}">${t.threadName}</a>
                            </div>
                            <div class="col-md-2 date">
                                <p>Created on: ${t.createdDate}</p>
                            </div>
                        </div>
                    </c:forEach>
                </c:if>
                <c:if test="${requestScope.searchId==2}">
                    <h3>Review search result:</h3>
                    <c:if test="${requestScope.data2 == null}"><h3>No result</h3></c:if>
                    <c:forEach items="${requestScope.data2}" var="m">
                        <div class="row line">
                            <div class="col-md-7 link">
                                <a href="reviewDetail?mid=${m.musicID}&ruid=0">${m.name}</a>
                            </div>
                            <div class="col-md-3 date">
                                <p>Composer: ${m.composerID.composerName}</p>
                            </div>
                            <div class="col-md-2 searchMark">
                                <span>${m.strMark}</span>
                                <span><i class="fa-solid fa-star"></i></span>
                            </div>
                        </div>
                    </c:forEach>
                </c:if>

            </div>
        </div>
    </body>
</html>
