<%-- 
    Document   : ReviewList
    Created on : Jul 10, 2022, 8:50:15 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <link rel="stylesheet" href="CSS/header.css">
        <link rel="stylesheet" href="CSS/reviewlist.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
        <script type="text/javascript">
            function doDelete(id) {
                if (confirm("Are you sure want to delete this song?")) {
                    window.location = "deleteSong?mid=" + id;
                }
            }
        </script>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <c:if test="${sessionScope.account != null}">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-dark" id="newThreadBtn" href="addSong">Add a new song</a>  
                    </div>
                </div>
            </div>
        </c:if>

        <div class="container-md sortForm">
            <form action="review">
                <label for="sortid">Sort By:</label>

                <select id="sortid" name="sortid">
                    <option value="1">Default</option>
                    <option value="2">Highest Mark</option>
                    <option value="3">Lowest Mark</option>
                    <option value="4">Name</option>
                </select>
                <input type="submit" class="btn btn-dark" value="SORT"/>
            </form>
        </div>

        <div class="container-md">
            <div class="row">
                <c:forEach items="${requestScope.data}" var="m"> 
                    <div class="col-xs-12 col-sm-12 col-md-9 mx-auto song">
                        <div class="row test3">
                            <div class="col-md-2 artworkWrap"> <img class="artwork" src="${m.artwork}" alt=""/></div>
                            <div class="row col-md-10">
                                <div class="col-md-6 songname"><a href="reviewDetail?mid=${m.musicID}&ruid=0">${m.name}</a></div>
                                <div class="col-md-6 genre"><p>${m.genre}</p></div>
                                <div class="col-md-6 composerName"><p>${m.composerID.composerName}</p></div>
                                <div class="col-md-3 playButton"><a target=”_blank” href="${m.link}">Play</a></div>
                                <div class="col-md-3 mark">
                                    <span>${m.strMark}</span>
                                    <i class="fa-solid fa-star"></i>
                                </div>
                                <c:if test="${m.uploaderID.username == sessionScope.account.username}">
                                    <div class="col-md-12 deleteButton"><a href="#" onclick="doDelete('${m.musicID}')">[Delete]</a></div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
