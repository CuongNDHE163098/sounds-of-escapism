<%-- 
    Document   : reviewDetail
    Created on : Jul 11, 2022, 1:52:51 AM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <link rel="stylesheet" href="CSS/header.css">
        <link rel="stylesheet" href="CSS/reviewdetail.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <link rel="stylesheet" href="CSS/star.css">
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <br/>

        

        <div class="container-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-4 artwork">
                    <img src="${requestScope.song.artwork}" alt="alt"/>
                    <br/>
                    <div class="avgMark">
                        <span>${requestScope.mark}</span>
                        <span><i class="fa-solid fa-star"></i></span>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-8 detail">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-md-7">
                            <h2 class="songName">${requestScope.song.name}</h2>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-4 uploader">
                            <h3>Uploaded by: ${requestScope.song.uploaderID.username}</h3>
                        </div>
                        <c:if test="${sessionScope.account.username == requestScope.song.uploaderID.username}">
                            <div class="col-sm-12 col-xs-12 col-md-1 edit">
                                <a href="updateSong?mid=${requestScope.song.musicID}">[Edit]</a>
                            </div>
                        </c:if>
                    </div>
                    <hr>

                    <h3 class="composerName">Artist: ${requestScope.song.composerID.composerName}</h3>
                    <h3 class="genre">Genre: ${requestScope.song.genre}</h3>
                    <h3 class="description">Description</h3>
                    <p class="desc">${requestScope.song.desc}</p>
                    <div class="playButton"><a target=”_blank” href="${requestScope.song.link}">Play</a></div>
                </div>
            </div>
        </div>

        <hr>
        <div class="container-md">
            <div class="row">
                <div class="AOR">${requestScope.aor} Reviews</div>
                <c:forEach items="${requestScope.data}" var="r">
                    <c:if test="${r.reviewID == requestScope.ruid && r.reviewerID.username == sessionScope.account.username}">
                        <div>
                            <h3>
                                Edit your review: 
                            </h3>
                            <span class="usernameRV">${sessionScope.account.username}</span>
                        </div>
                        <form action="updateReview" method="post">
                            <!--                    <input type="text" class="form-control rvContentAdd" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="content" autocomplete="off">-->
                            <textarea name="reviewContent" class="descBox">${r.content}</textarea>
                            <div class="rating-wrap">
                                <div class="center">
                                    <fieldset class="rating">
                                        <input type="radio" id="star5" name="rating" value="5"/><label for="star5" class="full" title="Awesome"></label>
                                        <input type="radio" id="star4.5" name="rating" value="4.5"/><label for="star4.5" class="half"></label>
                                        <input type="radio" id="star4" name="rating" value="4"/><label for="star4" class="full"></label>
                                        <input type="radio" id="star3.5" name="rating" value="3.5"/><label for="star3.5" class="half"></label>
                                        <input type="radio" id="star3" name="rating" value="3"/><label for="star3" class="full"></label>
                                        <input type="radio" id="star2.5" name="rating" value="2.5"/><label for="star2.5" class="half"></label>
                                        <input type="radio" id="star2" name="rating" value="2"/><label for="star2" class="full"></label>
                                        <input type="radio" id="star1.5" name="rating" value="1.5"/><label for="star1.5" class="half"></label>
                                        <input type="radio" id="star1" name="rating" value="1"/><label for="star1" class="full"></label>
                                        <input type="radio" id="star0.5" name="rating" value="0.5"/><label for="star0.5" class="half"></label>
                                    </fieldset>
                                </div>

                                <h4 id="rating-value"></h4>
                            </div>

                            <input type="hidden" name="username" value="${r.reviewerID.username}"/>
                            <input type="hidden" name="rid" value="${r.reviewID}"/>
                            <input type="hidden" name="mid" value="${requestScope.song.musicID}"/>

                            <button type="submit" class="editbtn btn btn-dark">Edit</button>
                        </form>
                    </c:if>
                    <c:if test="${r.reviewID == requestScope.ruid && r.reviewerID.username != sessionScope.account.username}">
                        <h3>You need to login to edit your comment</h3>
                    </c:if>
                    <c:if test="${r.reviewID != requestScope.ruid}">
                        <div class="reviewWrap">
                            <div class="reviewHeader">
                                <div class="row">
                                    <div class="col-6">
                                        <span class="reviewerUser">${r.reviewerID.username}</span>
                                        <span>rated this song </span>
                                        <span class="Mark">${r.mark}</span>
                                        <span><i class="fa-solid fa-star"></i></span>
                                    </div>
                                    <div class="col-6 editRv">
                                        <c:if test="${r.reviewerID.username == sessionScope.account.username && requestScope.ruid == 0}">
                                            <a class="updateBtn" href="reviewDetail?mid=${requestScope.song.musicID}&ruid=${r.reviewID}">Update this review</a>
                                            <a class="deleteRvBtn" href="deleteReview?rid=${r.reviewID}&mid=${requestScope.song.musicID}">Delete this review</a>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                            <p class="rvContent">${r.content}</p>
                        </div>
                        <br/>
                    </c:if>
                </c:forEach>
            </div>
            <c:if test="${sessionScope.account == null && ruid == 0}">
                <h3>You need to login to add your review</h3>
            </c:if>
            <c:if test="${sessionScope.account != null && ruid == 0}">
                <span>Add your review here</span>
                <span class="usernameRV">${sessionScope.account.username}</span>
                <form action="addReview" method="post">
                    <!--                    <input type="text" class="form-control rvContentAdd" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="content" autocomplete="off">-->
                    <textarea name="reviewContent" class="descBox"></textarea>
                    <div class="rating-wrap">
                        <div class="center">
                            <fieldset class="rating">
                                <input type="radio" id="star5" name="rating" value="5"/><label for="star5" class="full" title="Awesome"></label>
                                <input type="radio" id="star4.5" name="rating" value="4.5"/><label for="star4.5" class="half"></label>
                                <input type="radio" id="star4" name="rating" value="4"/><label for="star4" class="full"></label>
                                <input type="radio" id="star3.5" name="rating" value="3.5"/><label for="star3.5" class="half"></label>
                                <input type="radio" id="star3" name="rating" value="3"/><label for="star3" class="full"></label>
                                <input type="radio" id="star2.5" name="rating" value="2.5"/><label for="star2.5" class="half"></label>
                                <input type="radio" id="star2" name="rating" value="2"/><label for="star2" class="full"></label>
                                <input type="radio" id="star1.5" name="rating" value="1.5"/><label for="star1.5" class="half"></label>
                                <input type="radio" id="star1" name="rating" value="1"/><label for="star1" class="full"></label>
                                <input type="radio" id="star0.5" name="rating" value="0.5"/><label for="star0.5" class="half"></label>
                            </fieldset>
                        </div>

                        <h4 id="rating-value"></h4>
                    </div>

                    <input type="hidden" name="username" value="${sessionScope.account.username}"/>
                    <input type="hidden" name="mid" value="${requestScope.song.musicID}"/>
                    <button type="submit" class="submitbtn btn btn-dark">Submit</button>
                </form>
            </c:if>
        </div>
        <script src="Javascript/star-ratings.js"></script>
    </body>
</html>
