<%-- 
    Document   : test
    Created on : Jul 6, 2022, 4:43:57 PM
    Author     : ACER
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/home.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="hugeBanner">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-centered">
                        <div class="maintxt">
                            <img src="images/Homepage background gradiented 2.jpg" class="img-responsive bg-img"/> 
                            <a class="hotNews" href="#">Homepage</a>
                            <p class="overlay-text-large">Sounds Of Escapism</p>
                            <p class="overlay-text-small">This website provides you with music-based forums with different threads from users,</p>
                            <p class="overlay-text-small1"> also allows you to read reviews of songs and album and write your own reviews as well.</p>
                            <a class="overlay-text-small2" href="threadList" >Forum</a>
                            <a class="overlay-text-small3" href="review">Review</a>
                            <p class="overlay-text-small4">Creator</p>
                            <img class="overlay-image rounded-circle" src="images/Duc Cuong.jpg" alt="alt"/>
                            <p class="overlay-text-small5">Nguyen Duc Cuong</p>
                            <p class="overlay-text-small6">FPT University</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/>



        <div class="container-md">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 cbestSong">
                    <h2>Highest Rated Songs:</h2>
                </div>
            </div>
            <div class="row bestSongs">
                <c:forEach items="${requestScope.bestSong}" var="m">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="song">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <a href="reviewDetail?mid=${m.musicID}&ruid=0" ><img src="${m.artwork}" alt="alt"/></a>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 songDetail">
                                    <a href="reviewDetail?mid=${m.musicID}&ruid=0">
                                        <h2>${m.name}</h2>
                                    </a>
                                    <span class="markStar">${m.strMark}</span>
                                    <span><i class="fa-solid fa-star"></i></span>
                                    <br/>
                                    <br/>
                                    <div class="playBtn">
                                        <a target=”_blank” href="${m.link}">Play</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>

        <div class="container"> 
            <div class="row">
                <div class="startHere col-xs-12 col-sm-12 col-md-12 col-centered"> You can start here:</div>
            </div>
        </div>

        <br/>
        <div class="container">
            <div class="loginBanner">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-centered">
                        <img src="images/leaves-dark.png" alt="alt" class="img-responsive loginBg"/>
                        <c:if test="${sessionScope.account == null}">
                            <a class="loginBtn" href="login">Login</a>
                            <a class="registerBtn" href="addAccount">Register</a>
                        </c:if>

                        <c:if test="${sessionScope.account != null}">
                            <a class="helloUsername" href="userInfo?username=${sessionScope.account.username}">Hello "${sessionScope.account.name}"</a>
                            <form action="logout" method="post">
                                <button class="logoutBtn" type="submit">Logout</button>
                            </form>
                            <a class="forumBtn" href="threadList">Forum</a>
                            <a class="reviewBtn" href="review?sortid=1">Review</a>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>
