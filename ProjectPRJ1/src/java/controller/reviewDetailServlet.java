/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.util.List;
import model.Music;
import model.Review;

/**
 *
 * @author ACER
 */
@WebServlet(name = "reviewDetailServlet", urlPatterns = {"/reviewDetail"})
public class reviewDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet reviewDetailServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet reviewDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mid_raw = request.getParameter("mid");
        String ruid_raw = request.getParameter("ruid");
        try {
            int mid = Integer.parseInt(mid_raw);
            int ruid = Integer.parseInt(ruid_raw);
            request.setAttribute("ruid", ruid);
            DAO d = new DAO();
            Music m = d.getMusicById(mid);
            request.setAttribute("song", m);
            double mark = d.getMarkByMusicID(mid);
            DecimalFormat numberFormat = new DecimalFormat("#.##");
            String mark_formatted = numberFormat.format(mark);
            request.setAttribute("mark", mark_formatted);

            List<Review> list = d.getReviewByMid(mid);
            int aor = list.size();
            request.setAttribute("data", list);
            request.setAttribute("aor", aor);
            request.getRequestDispatcher("reviewDetail.jsp").forward(request, response);

        } catch (NumberFormatException e) {

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
