/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Music;

/**
 *
 * @author ACER
 */
@WebServlet(name="updateSongServlet", urlPatterns={"/updateSong"})
public class updateSongServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateSongServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateSongServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String mid_raw = request.getParameter("mid");
        DAO d= new DAO();
        try{
            int mid = Integer.parseInt(mid_raw);
            Music m = d.getMusicById(mid);
            request.setAttribute("song", m);
            request.getRequestDispatcher("updateSong.jsp").forward(request, response);
        }catch(NumberFormatException e){
            System.out.println(e.getMessage());
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String mid_raw = request.getParameter("mid");
        String name = request.getParameter("name");
        String cid_raw = request.getParameter("composerID");
        String genre = request.getParameter("genre");
        String desc = request.getParameter("desc");
        String link = request.getParameter("link");
        String artwork = request.getParameter("image");
        String uploaderID = request.getParameter("uploaderID");
        DAO d = new DAO();
        try{
            int mid, cid;
            mid = Integer.parseInt(mid_raw);
            cid = Integer.parseInt(cid_raw);
            Music m = new Music(mid, name, link, d.getComposerByID(cid), d.getUserByName(uploaderID), desc, artwork, genre);
            d.updateSong(m);
            response.sendRedirect("reviewDetail?mid="+mid+"&ruid=0");
        }catch(NumberFormatException e){
            System.out.println(e.getMessage());
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
