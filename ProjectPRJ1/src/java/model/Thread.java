/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author ACER
 */
public class Thread {

    private int ThreadID;
    private String ThreadName;
    private User CreatorID;
    private Date createdDate;
    private int TotalReplies;

    public Thread() {
    }

    public Thread(int ThreadID, String ThreadName, User CreatorID, Date createdDate) {
        this.ThreadID = ThreadID;
        this.ThreadName = ThreadName;
        this.CreatorID = CreatorID;
        this.createdDate = createdDate;
    }

    public Thread(int ThreadID, String ThreadName, User CreatorID, Date createdDate, int TotalReplies) {
        this.ThreadID = ThreadID;
        this.ThreadName = ThreadName;
        this.CreatorID = CreatorID;
        this.createdDate = createdDate;
        this.TotalReplies = TotalReplies;
    }

    public int getTotalReplies() {
        return TotalReplies;
    }

    public void setTotalReplies(int TotalReplies) {
        this.TotalReplies = TotalReplies;
    }

    public int getThreadID() {
        return ThreadID;
    }

    public void setThreadID(int ThreadID) {
        this.ThreadID = ThreadID;
    }

    public String getThreadName() {
        return ThreadName;
    }

    public void setThreadName(String ThreadName) {
        this.ThreadName = ThreadName;
    }

    public User getCreatorID() {
        return CreatorID;
    }

    public void setCreatorID(User CreatorID) {
        this.CreatorID = CreatorID;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Thread{" + "ThreadID=" + ThreadID + ", ThreadName=" + ThreadName + ", CreatorID=" + CreatorID + ", createdDate=" + createdDate + '}';
    }

}
