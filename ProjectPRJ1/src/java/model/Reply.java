/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author ACER
 */
public class Reply {
    private int ReplyID;
    private User username;
    private Timestamp Time;
    private String Content;

    public Reply() {
    }

    public Reply(int ReplyID, User username, Timestamp Time, String Content) {
        this.ReplyID = ReplyID;
        this.username = username;
        this.Time = Time;
        this.Content = Content;
    }

    public int getReplyID() {
        return ReplyID;
    }

    public void setReplyID(int ReplyID) {
        this.ReplyID = ReplyID;
    }

    public User getUsername() {
        return username;
    }

    public void setUsername(User username) {
        this.username = username;
    }

    public Timestamp getTime() {
        return Time;
    }

    public void setTime(Timestamp Time) {
        this.Time = Time;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    @Override
    public String toString() {
        return "Reply{" + "ReplyID=" + ReplyID + ", username=" + username + ", Time=" + Time + ", Content=" + Content + '}';
    }
    
    
    
}
