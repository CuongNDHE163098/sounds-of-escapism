/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ACER
 */
public class Review {

    private int ReviewID;
    private String Content;
    private User reviewerID;
    private double Mark;

    public Review() {
    }

    public Review(int ReviewID, String Content, User reviewerID, double Mark) {
        this.ReviewID = ReviewID;
        this.Content = Content;
        this.reviewerID = reviewerID;
        this.Mark = Mark;
    }

    public int getReviewID() {
        return ReviewID;
    }

    public void setReviewID(int ReviewID) {
        this.ReviewID = ReviewID;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public User getReviewerID() {
        return reviewerID;
    }

    public void setReviewerID(User reviewerID) {
        this.reviewerID = reviewerID;
    }

    public double getMark() {
        return Mark;
    }

    public void setMark(double Mark) {
        this.Mark = Mark;
    }

    @Override
    public String toString() {
        return "Review{" + "ReviewID=" + ReviewID + ", Content=" + Content + ", reviewerID=" + reviewerID + ", Mark=" + Mark + '}';
    }

}
