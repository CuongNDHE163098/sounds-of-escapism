/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ACER
 */
public class Composer {

    private int ComposerID;
    private String Desc;
    private User AdderID;
    private String photo;
    private String ComposerName;

    public Composer() {
    }

    public Composer(int ComposerID, String Desc, User AdderID, String photo, String ComposerName) {
        this.ComposerID = ComposerID;
        this.Desc = Desc;
        this.AdderID = AdderID;
        this.photo = photo;
        this.ComposerName = ComposerName;
    }

    public int getComposerID() {
        return ComposerID;
    }

    public void setComposerID(int ComposerID) {
        this.ComposerID = ComposerID;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String Desc) {
        this.Desc = Desc;
    }

    public User getAdderID() {
        return AdderID;
    }

    public void setAdderID(User AdderID) {
        this.AdderID = AdderID;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getComposerName() {
        return ComposerName;
    }

    public void setComposerName(String ComposerName) {
        this.ComposerName = ComposerName;
    }

    @Override
    public String toString() {
        return "Composer{" + "ComposerID=" + ComposerID + ", Desc=" + Desc + ", AdderID=" + AdderID + ", photo=" + photo + ", ComposerName=" + ComposerName + '}';
    }

}
