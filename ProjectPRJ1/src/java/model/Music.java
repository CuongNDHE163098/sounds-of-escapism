/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ACER
 */
public class Music {

    private int MusicID;
    private String Name;
    private String Link;
    private Composer ComposerID;
    private User UploaderID;
    private String Desc;
    private String Artwork;
    private String Genre;
    private double AvgMark;
    private String StrMark;

    public Music(int MusicID, String Name, String Link, Composer ComposerID, User UploaderID, String Desc, String Artwork, String Genre, double AvgMark) {
        this.MusicID = MusicID;
        this.Name = Name;
        this.Link = Link;
        this.ComposerID = ComposerID;
        this.UploaderID = UploaderID;
        this.Desc = Desc;
        this.Artwork = Artwork;
        this.Genre = Genre;
        this.AvgMark = AvgMark;
    }

    public Music() {
    }

    public Music(int MusicID, String Name, String Link, Composer ComposerID, User UploaderID, String Desc, String Artwork, String Genre) {
        this.MusicID = MusicID;
        this.Name = Name;
        this.Link = Link;
        this.ComposerID = ComposerID;
        this.UploaderID = UploaderID;
        this.Desc = Desc;
        this.Artwork = Artwork;
        this.Genre = Genre;
    }

    public Music(int MusicID, String Name, String Link, Composer ComposerID, User UploaderID, String Desc, String Artwork, String Genre, String StrMark) {
        this.MusicID = MusicID;
        this.Name = Name;
        this.Link = Link;
        this.ComposerID = ComposerID;
        this.UploaderID = UploaderID;
        this.Desc = Desc;
        this.Artwork = Artwork;
        this.Genre = Genre;
        this.StrMark = StrMark;
    }

    public String getStrMark() {
        return StrMark;
    }

    public void setStrMark(String StrMark) {
        this.StrMark = StrMark;
    }

    public double getAvgMark() {
        return AvgMark;
    }

    public void setAvgMark(double AvgMark) {
        this.AvgMark = AvgMark;
    }

    public int getMusicID() {
        return MusicID;
    }

    public void setMusicID(int MusicID) {
        this.MusicID = MusicID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String Link) {
        this.Link = Link;
    }

    public Composer getComposerID() {
        return ComposerID;
    }

    public void setComposerID(Composer ComposerID) {
        this.ComposerID = ComposerID;
    }

    public User getUploaderID() {
        return UploaderID;
    }

    public void setUploaderID(User UploaderID) {
        this.UploaderID = UploaderID;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String Desc) {
        this.Desc = Desc;
    }

    public String getArtwork() {
        return Artwork;
    }

    public void setArtwork(String Artwork) {
        this.Artwork = Artwork;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String Genre) {
        this.Genre = Genre;
    }

    @Override
    public String toString() {
        return "Music{" + "MusicID=" + MusicID + ", Name=" + Name + ", Link=" + Link + ", ComposerID=" + ComposerID + ", UploaderID=" + UploaderID + ", Desc=" + Desc + ", Artwork=" + Artwork + ", Genre=" + Genre + '}';
    }

}
