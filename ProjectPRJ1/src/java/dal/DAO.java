/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Composer;
import model.Music;
import model.Reply;
import model.Review;
import model.Thread;
import model.User;

/**
 *
 * @author ACER
 */
public class DAO extends DBContext {

    public List<Thread> getAllThreads() {
        List<Thread> list = new ArrayList<>();
        String sql = "select * from Thread";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Thread t = new Thread(rs.getInt("ThreadID"), rs.getString("ThreadName"), getUserByName(rs.getString("CreatorID")), rs.getDate("createdDate"));
                list.add(t);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public User getUserByName(String username) {
        String sql = "SELECT * FROM [User] Where username = '" + username + "'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(username, rs.getString("password"), rs.getString("name"), rs.getDate("DOB"), rs.getInt("Role"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<Reply> getReplyByThreadID(int ThreadID) {
        List<Reply> list = new ArrayList<>();
        String sql = "SELECT Thread.ThreadID, ThreadName, CreatorID, Reply.ReplyID, createdDate, username, [Time], Content\n"
                + "FROM Thread \n"
                + "JOIN Thread_Reply \n"
                + "ON Thread.ThreadID = Thread_Reply.ThreadID\n"
                + "JOIN Reply \n"
                + "ON Thread_reply.ReplyID = Reply.ReplyID\n"
                + "WHERE Thread.ThreadID = " + ThreadID;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Reply r = new Reply(rs.getInt("ReplyID"), getUserByName(rs.getString("username")), rs.getTimestamp("Time"), rs.getString("Content"));
                list.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Thread getThreadById(int id) {
        String sql = "SELECT * FROM Thread WHERE ThreadID = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Thread t = new Thread(id, rs.getString("ThreadName"), getUserByName(rs.getString("CreatorID")), rs.getDate("createdDate"));
                return t;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void addComment(String content, User username, int ThreadID) {
        try {
            //insert into reply
            int ReplyID = getMaxReplyId() + 1;
            Date date = new Date();
            Timestamp currentTime = new Timestamp(date.getTime());
            String sql = "INSERT INTO [dbo].[Reply]\n"
                    + "           ([ReplyID]\n"
                    + "           ,[username]\n"
                    + "           ,[Time]\n"
                    + "           ,[Content])\n"
                    + "     VALUES(?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ReplyID);
            st.setString(2, username.getUsername());
            st.setTimestamp(3, currentTime);
            st.setString(4, content);
            st.executeUpdate();

            //insert into thread
            String sql1 = "INSERT INTO [dbo].[Thread_Reply]\n"
                    + "           ([ThreadID]\n"
                    + "           ,[ReplyID])\n"
                    + "           VALUES(?,?)";

            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, ThreadID);
            st1.setInt(2, ReplyID);
            st1.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public int getMaxReplyId() {
        String sql = "SELECT MAX(ReplyID) [MaxID]\n"
                + "FROM Reply";
        try {
            int maxid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                maxid = rs.getInt("MaxID");
                return maxid;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public int getMaxThreadId() {
        String sql = "SELECT MAX(ThreadID) MaxID FROM Thread";
        try {
            int maxid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                maxid = rs.getInt("MaxID");
                return maxid;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public User loginCheck(String username, String password) {
        String sql = "SELECT [username]\n"
                + "      ,[password]\n"
                + "      ,[Name]\n"
                + "      ,[DOB]\n"
                + "      ,[Role]\n"
                + "  FROM [dbo].[User]"
                + "  WHERE username = '" + username + "' and [password] = " + password;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(username, password, rs.getString("Name"), rs.getDate("DOB"), rs.getInt("Role"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void addThread(String ThreadName, User username) {
        try {
            //insert into thread
            int ThreadID = getMaxThreadId() + 1;
            Date date = new Date();
            Timestamp currentTime = new Timestamp(date.getTime());
            String sql = "INSERT INTO [dbo].[Thread]\n"
                    + "           ([ThreadID]\n"
                    + "           ,[ThreadName]\n"
                    + "           ,[CreatorID]\n"
                    + "           ,[createdDate])\n"
                    + "     VALUES(?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ThreadID);
            st.setString(2, ThreadName);
            st.setString(3, username.getUsername());
            st.setTimestamp(4, currentTime);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public String addNewAccount(String username, String password, String Name, java.sql.Date DOB) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([username]\n"
                + "           ,[password]\n"
                + "           ,[Name]\n"
                + "           ,[DOB]\n"
                + "           ,[Role])\n"
                + "     VALUES (?,?,?,?,?)";
        if (usernameDup(username) == false) {
            try {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, username);
                st.setString(2, password);
                st.setString(3, Name);
                st.setDate(4, DOB);
                st.setInt(5, 2);
                st.executeUpdate();
                return "Success";
            } catch (SQLException e) {
                return e.getMessage();
            }
        } else {
            return "Failed, Username is taken";
        }
    }

    public boolean usernameDup(String username) {
        String sql = " SELECT * FROM [User]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (username.equals(rs.getString("username"))) {
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public List<Music> getAllMusic() {
        List<Music> list = new ArrayList<>();
        String sql = "select * from Music";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Music m = new Music(rs.getInt("MusicID"), rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public List<Music> getMusicOrderedByMark() {
        List<Music> list = new ArrayList<>();
        String sql = "SELECT m.MusicID, [Name], Link, ComposerID, UploaderID, [Desc], Artwork, Genre, Mark FROM Music m LEFT JOIN(\n"
                + "SELECT m.MusicID, AVG(Mark) Mark FROM Music m\n"
                + "JOIN Post_Review pr ON m.MusicID = pr.MusicID\n"
                + "JOIN Review r ON pr.ReviewID = r.ReviewID\n"
                + "GROUP BY m.MusicID) t1 ON m.MusicID = t1.MusicID\n"
                + "ORDER BY Mark Desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Music m = new Music(rs.getInt("MusicID"), rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"), "");
                m.setStrMark(getMarkStrByMusicID(m.getMusicID()));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public List<Music> getMusicOrderedByLowMark() {
        List<Music> list = new ArrayList<>();
        String sql = "SELECT m.MusicID, [Name], Link, ComposerID, UploaderID, [Desc], Artwork, Genre, Mark FROM Music m LEFT JOIN(\n"
                + "SELECT m.MusicID, AVG(Mark) Mark FROM Music m\n"
                + "JOIN Post_Review pr ON m.MusicID = pr.MusicID\n"
                + "JOIN Review r ON pr.ReviewID = r.ReviewID\n"
                + "GROUP BY m.MusicID) t1 ON m.MusicID = t1.MusicID\n"
                + "ORDER BY Mark Asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Music m = new Music(rs.getInt("MusicID"), rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"), "");
                m.setStrMark(getMarkStrByMusicID(m.getMusicID()));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public List<Music> getMusicOrderedByName() {
        List<Music> list = new ArrayList<>();
        String sql = "SELECT MusicID, [Name], Link, t2.ComposerID, UploaderID, t2.[Desc], Artwork, Genre, c.ComposerName, Mark  FROM Composer c JOIN\n"
                + "(SELECT m.MusicID, [Name], Link, ComposerID, UploaderID, [Desc], Artwork, Genre, Mark FROM Music m LEFT JOIN(\n"
                + "SELECT m.MusicID, AVG(Mark) Mark FROM Music m\n"
                + "JOIN Post_Review pr ON m.MusicID = pr.MusicID\n"
                + "JOIN Review r ON pr.ReviewID = r.ReviewID\n"
                + "GROUP BY m.MusicID) t1 ON m.MusicID = t1.MusicID) t2 ON c.ComposerID = t2.ComposerID\n"
                + "ORDER BY [Name] Asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Music m = new Music(rs.getInt("MusicID"), rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"), "");
                m.setStrMark(getMarkStrByMusicID(m.getMusicID()));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Composer getComposerByID(int Id) {
        String sql = "SELECT * FROM Composer WHERE ComposerID = " + Id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Composer c = new Composer(Id, rs.getString("Desc"), getUserByName(rs.getString("AdderID")), rs.getString("Photo"), rs.getString("ComposerName"));
                return c;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public double getMarkByMusicID(int MusicID) {
        String sql = "SELECT * FROM\n"
                + "(SELECT m.MusicID, AVG(Mark) Mark FROM Music m\n"
                + "JOIN Post_Review pr ON m.MusicID = pr.MusicID\n"
                + "JOIN Review r ON pr.ReviewID = r.ReviewID\n"
                + "GROUP BY m.MusicID) t1 WHERE t1.MusicID = " + MusicID;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                double mark = rs.getFloat("Mark");
                return mark;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public String getMarkStrByMusicID(int MusicID) {
        double markDouble = getMarkByMusicID(MusicID);
        DecimalFormat formatter = new DecimalFormat("#.##");
        String markStr = formatter.format(markDouble);
        return markStr;
    }

    public Music getMusicById(int MusicID) {
        String sql = "SELECT * FROM Music where MusicID = " + MusicID;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Music m = new Music(MusicID, rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"));
                return m;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<Review> getReviewByMid(int Mid) {
        String sql = "SELECT r.ReviewID, Content, ReviewerID, Mark, MusicID FROM Review r JOIN Post_Review pr ON r.ReviewID = pr.ReviewID WHERE musicID = " + Mid;
        List<Review> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Review r = new Review(rs.getInt("ReviewID"), rs.getString("Content"), getUserByName(rs.getString("ReviewerID")), rs.getFloat("Mark"));
                list.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public void updateSong(Music m) {
        String sql = "UPDATE [dbo].[Music]\n"
                + "   SET [MusicID] = ?\n"
                + "      ,[Name] = ?\n"
                + "      ,[Link] = ?\n"
                + "      ,[ComposerID] = ?\n"
                + "      ,[UploaderID] = ?\n"
                + "      ,[Desc] = ?\n"
                + "      ,[Artwork] = ?\n"
                + "      ,[Genre] = ?\n"
                + " WHERE MusicID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, m.getMusicID());
            st.setString(2, m.getName());
            st.setString(3, m.getLink());
            st.setInt(4, m.getComposerID().getComposerID());
            st.setString(5, m.getUploaderID().getUsername());
            st.setString(6, m.getDesc());
            st.setString(7, m.getArtwork());
            st.setString(8, m.getGenre());
            st.setInt(9, m.getMusicID());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void addReview(int mid, String username, String content, float mark) {
        try {
            //insert into review
            int ReviewID = getMaxReviewID() + 1;

            String sql = "INSERT INTO [dbo].[Review]\n"
                    + "           ([ReviewID]\n"
                    + "           ,[Content]\n"
                    + "           ,[ReviewerID]\n"
                    + "           ,[Mark])\n"
                    + "     VALUES(?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ReviewID);
            st.setString(2, content);
            st.setString(3, username);
            st.setFloat(4, mark);
            st.executeUpdate();

            //insert into post_review
            String sql1 = "INSERT INTO [dbo].[Post_Review]\n"
                    + "           ([MusicID]\n"
                    + "           ,[ReviewID])\n"
                    + "     VALUES(?,?)";

            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, mid);
            st1.setInt(2, ReviewID);
            st1.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public int getMaxReviewID() {
        String sql = "SELECT MAX(ReviewID) MaxID FROM Review";
        int maxid = -1;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                maxid = rs.getInt("MaxID");
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return maxid;
    }

    public List<Composer> getAllComposers() {
        List<Composer> list = new ArrayList<>();
        String sql = "SELECT * FROM Composer;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Composer c = new Composer(rs.getInt("ComposerID"), rs.getString("Desc"), getUserByName(rs.getString("AdderID")), rs.getString("Photo"), rs.getString("ComposerName"));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public void addNewSong(String name, String link, int composerID, String uploaderID, String desc, String artwork, String genre) {
        int MusicID = getMaxMusicID() + 1;
        String sql = "INSERT INTO [dbo].[Music]\n"
                + "           ([MusicID]\n"
                + "           ,[Name]\n"
                + "           ,[Link]\n"
                + "           ,[ComposerID]\n"
                + "           ,[UploaderID]\n"
                + "           ,[Desc]\n"
                + "           ,[Artwork]\n"
                + "           ,[Genre])\n"
                + "     VALUES(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, MusicID);
            st.setString(2, name);
            st.setString(3, link);
            st.setInt(4, composerID);
            st.setString(5, uploaderID);
            st.setString(6, desc);
            st.setString(7, artwork);
            st.setString(8, genre);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public int getMaxMusicID() {
        String sql = "SELECT MAX(MusicID) MaxID FROM Music";
        int maxid = -1;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                maxid = rs.getInt("MaxID");
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return maxid;
    }

    public int getMaxComposerId() {
        String sql = "SELECT MAX(ComposerID) MaxID FROM Composer";
        int maxid = -1;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                maxid = rs.getInt("MaxID");
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return maxid;
    }

    public void addNewArtist(String ComposerName, String desc, String photo, String AdderID) {
        String sql = "INSERT INTO [dbo].[Composer]\n"
                + "           ([ComposerID]\n"
                + "           ,[Desc]\n"
                + "           ,[AdderID]\n"
                + "           ,[Photo]\n"
                + "           ,[ComposerName])\n"
                + "     VALUES(?,?,?,?,?)";
        try {
            int composerID = getMaxComposerId() + 1;
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, composerID);
            st.setString(2, desc);
            st.setString(3, AdderID);
            st.setString(4, photo);
            st.setString(5, ComposerName);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteSong(int mid) {
        try {
            String sql = "DELETE FROM [dbo].[Music]\n"
                    + "      WHERE MusicID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, mid);
            st.executeUpdate();
            String sql1 = "DELETE FROM [dbo].[Post_Review]\n"
                    + "      WHERE MusicID=?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, mid);
            st1.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Thread> searchThread(String threadName, String CreatorID, java.sql.Date fromDate, java.sql.Date toDate) {
        List<Thread> list = new ArrayList<>();
        String sql = "SELECT [ThreadID]\n"
                + "      ,[ThreadName]\n"
                + "      ,[CreatorID]\n"
                + "      ,[createdDate]\n"
                + "  FROM [dbo].[Thread] WHERE 1=1 ";
        if (threadName != null && !threadName.equals("")) {
            sql += " and ThreadName like '%" + threadName + "%'";
        }
        if (CreatorID != null && !CreatorID.equals("")) {
            sql += " and CreatorID like '%" + CreatorID + "%'";
        }
        if (fromDate != null) {
            sql += " and createdDate>='" + fromDate + "'";
        }
        if (toDate != null) {
            sql += " and createdDate<='" + toDate + "'";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Thread t = new Thread(rs.getInt("ThreadID"), rs.getString("ThreadName"), getUserByName(rs.getString("CreatorID")), rs.getDate("createdDate"));
                list.add(t);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public List<Music> searchMusic(String songName, String composerName, String genre, int mark) {
        List<Music> list = new ArrayList<>();
        String sql = "SELECT MusicID, [Name], Link, t2.ComposerID, UploaderID, t2.[Desc], Artwork, Genre, c.ComposerName, Mark  FROM Composer c JOIN\n"
                + "(SELECT m.MusicID, [Name], Link, ComposerID, UploaderID, [Desc], Artwork, Genre, Mark FROM Music m LEFT JOIN(\n"
                + "SELECT m.MusicID, AVG(Mark) Mark FROM Music m\n"
                + "JOIN Post_Review pr ON m.MusicID = pr.MusicID\n"
                + "JOIN Review r ON pr.ReviewID = r.ReviewID\n"
                + "GROUP BY m.MusicID) t1 ON m.MusicID = t1.MusicID) t2 ON c.ComposerID = t2.ComposerID\n"
                + "WHERE 1=1 ";
        if (songName != null && !songName.equals("")) {
            sql += " and [Name] like '%" + songName + "%'";
        }

        if (composerName != null && !composerName.equals("")) {
            sql += " and ComposerName like '%" + composerName + "%'";
        }

        if (genre != null && !genre.equals("")) {
            sql += " and Genre like '%" + genre + "%'";
        }

        if (mark == 0) {
            sql += "";
        } else if (mark == 1) {
            sql += " and Mark >= 0 and Mark <=1";
        } else if (mark == 2) {
            sql += " and Mark >= 1 and Mark <=2";
        } else if (mark == 3) {
            sql += " and Mark >= 2 and Mark <=3";
        } else if (mark == 4) {
            sql += " and Mark >= 3 and Mark <=4";
        } else if (mark == 5) {
            sql += " and Mark >= 4 and Mark <=5";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Music m = new Music(rs.getInt("MusicID"), rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"), "");
                m.setStrMark(getMarkStrByMusicID(m.getMusicID()));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public void updateReview(int ReviewID, String Content, String username, float Mark) {
        String sql = "UPDATE [dbo].[Review]\n"
                + "   SET [ReviewID] = ?\n"
                + "      ,[Content] = ?\n"
                + "      ,[ReviewerID] = ?\n"
                + "      ,[Mark] = ?\n"
                + " WHERE ReviewID =" + ReviewID;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ReviewID);
            st.setString(2, Content);
            st.setString(3, username);
            st.setFloat(4, Mark);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteReview(int ReviewID) {
        try {
            //Delete out of Post_Review
            String sql1 = "DELETE FROM [dbo].[Post_Review]\n"
                    + "      WHERE ReviewID=" + ReviewID;
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.executeUpdate();

            //Delete out of Review
            String sql = "DELETE FROM [dbo].[Review]\n"
                    + "WHERE ReviewID=" + ReviewID;
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Music> bestSongs() {
        List<Music> list = new ArrayList<>();
        String sql = "SELECT m.MusicID, [Name], Link, ComposerID, UploaderID, [Desc], Artwork, Genre, Mark FROM Music m JOIN(\n"
                + "SELECT m.MusicID, AVG(Mark) Mark FROM Music m\n"
                + "JOIN Post_Review pr ON m.MusicID = pr.MusicID\n"
                + "JOIN Review r ON pr.ReviewID = r.ReviewID\n"
                + "GROUP BY m.MusicID) t1 ON m.MusicID = t1.MusicID\n"
                + "ORDER BY Mark Desc";
        int count = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (count < 4) {
                    Music m = new Music(rs.getInt("MusicID"), rs.getString("Name"), rs.getString("Link"), getComposerByID(rs.getInt("ComposerID")), getUserByName(rs.getString("UploaderID")), rs.getString("Desc"), rs.getString("Artwork"), rs.getString("Genre"), "");
                    m.setStrMark(getMarkStrByMusicID(m.getMusicID()));
                    list.add(m);
                    count++;
                } else {
                    break;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public void deleteReply(int rid) {
        try {
            //Delete Thread_Reply
            String sql = "DELETE FROM [dbo].[Thread_Reply]\n"
                    + "      WHERE ReplyID=" + rid;
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();

            String sql1 = "DELETE FROM [dbo].[Reply]\n"
                    + "      WHERE ReplyID=" + rid;
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.executeUpdate();
            //Delete Reply
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public int getTotalRepliesById(int tid) {
        String sql = "SELECT * FROM\n"
                + "(SELECT t.ThreadID, COUNT(ReplyID) TotalReplies FROM Thread t JOIN Thread_Reply tr ON t.ThreadID = tr.ThreadID\n"
                + "GROUP BY t.ThreadID) t1\n"
                + "WHERE t1.ThreadID = " + tid;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("TotalReplies");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public void deleteComposer(int cid) {
        try {
            //Delete Thread_Reply
            String sql = "DELETE FROM [dbo].[Composer]\n"
                    + "      WHERE ComposerID = " + cid;
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateComposer(int ComposerID, String desc, String AdderID, String Photo, String ComposerName) {
        String sql = "UPDATE [dbo].[Composer]\n"
                + "   SET [ComposerID] = ?\n"
                + "      ,[Desc] = ?\n"
                + "      ,[AdderID] = ?\n"
                + "      ,[Photo] = ?\n"
                + "      ,[ComposerName] = ?\n"
                + " WHERE ComposerID = " + ComposerID;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ComposerID);
            st.setString(2, desc);
            st.setString(3, AdderID);
            st.setString(4, Photo);
            st.setString(5, ComposerName);

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteThread(int tid) {
        try {
            String sql1 = "DELETE FROM [dbo].[Thread_Reply]\n"
                    + "      WHERE ThreadID=" + tid;
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.executeUpdate();
            
            String sql = "DELETE FROM [dbo].[Thread]\n"
                    + "      WHERE ThreadID=" + tid;
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        DAO d = new DAO();
        Date date = new Date();
        java.sql.Date currentSQLDate = new java.sql.Date(date.getTime());
        Timestamp currentTime = new Timestamp(date.getTime());
        //        List<Reply> list = d.getReplyByThreadID(2);
        //        for(Category C: list){
        //            System.out.println(C);
        //        }
        //        for (Reply r : list) {
        //            System.out.println(r);
        //        }
        //        System.out.println(d.getMaxReplyId());
        //        d.insert("Testing testing", d.getUserByName("cuongndhe"), 2);
        //        System.out.println(d.loginCheck("cuongndhe", "123"));
        //          d.addThread("Testcheck", d.getUserByName("cuongndhe"));
        //        System.out.println(d.addNewAccount("TestAcc", "123", "TestBro", currentSQLDate));
        //        List<Music> list = d.getAllMusic();
        //        for(Music m: list){
        //            System.out.println(m);
        //        }
        //        System.out.println(d.getMarkByMusicID(1));
        //        System.out.println(d.getMusicById(1));
        //        List<Review> list = d.getReviewByMid(1);
        //        for (Review r : list) {
        //            System.out.println(r);
        //        }
//        System.out.println(d.getMaxReviewID() + 1);
//        d.addReview(1, "cuongndhe", "Uhhhh i wrote this song so my opinion doesn't count", 3.5f);
//        d.addNewSong("MEGANTO METEOR", "https://www.youtube.com/watch?v=Lf5yC8c3daM&list=RDMMLf5yC8c3daM&start_radio=1", 2, "cuongndhe", "Lead song from Camellia's album MEGANTOR METEOR", "https://i.scdn.co/image/ab67616d0000b2735c47a5a1fe3380d3118aebb1", "Hardcore/Complextro");
//        System.out.println(d.getMaxComposerId());
//        d.addNewArtist("Foster The People", "Foster the People is an American indie pop band formed in Los Angeles, California, in 2009. Its members are singer Mark Foster, guitarist Sean Cimino, and keyboardist Isom Innis.", "https://www.billboard.com/wp-content/uploads/media/foster-the-people-2018-cr-Neil-Krug-billboard-1548.jpg", "cuongndhe");
//        List<Music> list = d.searchMusic("Camellia");
//        for (Music m : list) {
//            System.out.println(m);

//        }
//        List<Music> list = d.getAllMusic();
//        List<Music> list2 = new ArrayList<>();
//        for (Music m : list) {
//            m = new Music(m.getMusicID(), m.getName(), m.getLink(), m.getComposerID(), m.getUploaderID(), m.getDesc(), m.getArtwork(), m.getGenre(), "");
//            m.setStrMark(d.getMarkStrByMusicID(m.getMusicID()));
//            list2.add(m);
//        }
//
//        for (Music m : list2) {
//            System.out.println(m.getStrMark());
//        }
//        d.deleteReview(12);
//            for(Music m: d.bestSongs()){
//                System.out.println(m);
//                System.out.println("     " + m.getStrMark());
//            }
//        System.out.println(d.searchMusic("Never Be Like You", "", "", 0));
//        d.deleteComposer(7);
        d.deleteThread(9);
    }

}
