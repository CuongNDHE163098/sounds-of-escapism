<%-- 
    Document   : ArtistList
    Created on : Jul 11, 2022, 9:01:45 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/header.css">
        <link rel="stylesheet" href="CSS/artistList.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <c:if test="${sessionScope.account != null}">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-dark" id="newThreadBtn" href="addArtist">Add a new artist</a>  
                    </div>
                </div>
            </div>
        </c:if>

        <div class="container-md">
            <c:forEach items="${requestScope.data}" var="c">
                <div class="row composer">
                    <div class="col-xs-12 col-sm-12 col-md-2 photo">
                        <img src="${c.photo}" alt="alt"/>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 rightSide">
                        <div class="Name"><p>${c.composerName}</p></div>
                        <div class="composerID">composerID: ${c.composerID}</div>
                        <div class="desc">${c.desc}</div>

                        <div class="row">
                            <c:if test="${sessionScope.account.username == c.adderID.username}">
                                <div class=" col-1 updateBtn">
                                    <a href="updateComposer?cid=${c.composerID}">Update</a>
                                </div>
                            </c:if>
                            <c:if test="${sessionScope.account.username == c.adderID.username}">
                                <div class=" col-1 deleteBtn">
                                    <a href="deleteComposer?cid=${c.composerID}">Delete</a>
                                </div>
                            </c:if>
                        </div>
                    </div>

                </div>
            </c:forEach>
        </div>
    </body>
</html>
