<%-- 
    Document   : homepage
    Created on : Jun 15, 2022, 12:20:43 AM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/unused1.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for something...." aria-label="Search" aria-describedby="search-addon" />
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="homepage.jsp">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="#">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="#">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="#">About us</a>
                    </div>
                    <!--                    <div class="col-md-1 redir">
                                            <img class="smallLogo" src="images/001-facebook.png" alt="alt"/>
                                        </div>-->
                </div>
            </div>

            <!--            <div class="hugeBanner">
                            <img src="images/Homepage background gradiented.jpg" alt="alt" width="100%" height="650px"/>
                            <div class="textOnImg">Text</div>
                            <div class="top-left">Top left</div>
                            <div class="bottom-left">Bottom Left</div>
                            <div class="top-left">Top Left</div>
                            <div class="top-right">Top Right</div>
                            <div class="bottom-right">Bottom Right</div>
                            <div class="centered">Centered</div>
                        </div>-->
        </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
