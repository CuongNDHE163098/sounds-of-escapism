<%-- 
    Document   : addAccount
    Created on : Jul 9, 2022, 3:13:23 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/home.css">
        <link rel="stylesheet" href="CSS/threadDetail.css">
        <!--Inter font-->
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
    </head>
    <body>
        <div class="container-md">
            <div class="banner">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-1"><img src="images/music-logo-png-2350.png" alt="alt"/></div>
                    <!--search bar-->
                    <div class="col-md-4" id="searchBar">
                        <div class="input-group">
                            <form action="search" id="searchs">
                                <input type="search" class="form-control rounded searchType" placeholder="Search for forum...." aria-label="Search" aria-describedby="search-addon" name="threadName"/>
                                <!--                            <button type="button" class="btn btn-outline-primary">Search</button>-->
                                <input type="hidden" name="CreatorID" value=""/>
                                <input type="hidden" name="fromDate" value=""/>
                                <input type="hidden" name="toDate" value=""/>
                                <input type="hidden" name="sid" value="1"/>
                                <input type="submit" class="btn btn-outline-primary" value="Search">
                            </form>
                        </div>
                    </div>
                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Home</a>
                    </div>
                    <!--Forum-->
                    <div class="col-md-1 redir">
                        <a href="threadList">Forum</a>
                    </div>
                    <!--Review-->
                    <div class="col-md-1 redir">
                        <a href="review?sortid=1">Review</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="artistList">Artists</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-md">
            <h2 style="margin:10px 0px;">Fill in all of the required boxes</h2>
            <form action="addAccount" method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Username: </span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="username">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Password: </span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="password">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Your name: </span>
                    </div>

                    <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="Name">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span style="margin: 0 10px;"class="input-group-text" id="inputGroup-sizing-default">Date of birth: </span>
                    </div>
                    <input type="date" name="DOB"/>
                </div>

                <input type="hidden" name="username" value="${sessionScope.account.username}"/>
                <button type="submit" class="submitbtn btn btn-dark" style="margin:0px 10px;">Add Account</button>
            </form>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>
